using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace BitCom.Common.Mongo
{
    public class MongoInitializer :IDatabaseInitializer
    {
        private bool _initialized = false;
        private readonly bool _seed;
        private readonly IMongoDatabase _database;

        public MongoInitializer(IMongoDatabase pDataBase, IOptions<MongoOptions> pOptions)
        {
            _database = pDataBase;
        }
        public Task InitializeAsync()
        {
            if(_initialized)
                return Task.CompletedTask;
            RegisterConventions();
            //if (!_seed)
            _initialized = true;
            return Task.CompletedTask;
        }
        private void RegisterConventions()
        {
            ConventionRegistry.Register("BitComConventions",new MongoConventions(), x => true );   
        }

        private class MongoConventions : IConventionPack
        {
            public IEnumerable<IConvention> Conventions => new List<IConvention>
            {
                new IgnoreExtraElementsConvention(true),
                new EnumRepresentationConvention(BsonType.String),
                new CamelCaseElementNameConvention()
            };
        }
    }
}