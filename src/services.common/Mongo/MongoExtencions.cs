using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace BitCom.Common.Mongo
{
    public static class MongoExtencions
    {
        public static void AddMongoDB(this IServiceCollection pServices, IConfiguration pConfig)
        {
            pServices.Configure<MongoOptions>(pConfig.GetSection("mongo"));
            pServices.AddSingleton(c =>
            {
                var options = c.GetService<IOptions<MongoOptions>>();
                return new MongoClient(options.Value.ConnectionString);
            });
            pServices.AddScoped(c =>
            {
                var options = c.GetService<IOptions<MongoOptions>>();
                var client = c.GetService<MongoClient>();
                return client.GetDatabase(options.Value.Database);
            });
            pServices.AddScoped<IDatabaseInitializer, MongoInitializer>();
        }
    }
}