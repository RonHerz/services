using System.Threading.Tasks;

namespace BitCom.Common.Mongo
{
    public interface IDatabaseInitializer
    {
        Task InitializeAsync();
    }
}