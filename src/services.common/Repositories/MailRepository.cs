using System.Collections.Generic;
using System.Threading.Tasks;
using Cog.Common.Sql;

namespace Cog.Common.Repositories
{
    public class MailRepositoryBase  
    {
        public const int MAILBOX_DISABLE = 536870912;

        protected readonly ISqlActionsHandler _conn;

        public MailRepositoryBase(ISqlActionsHandler pConn)
        {
            _conn = pConn;
        }
        public async Task<bool> IsAllow(int idAccount)
        {
            var resp = await _conn.ExecuteScalarQueryAsync<int>("SELECT BitPermission FROM Customer WITH(NOLOCK) WHERE idCustomer = @AccountId", 
                new Dictionary<string, object> {
                    { "AccountId", idAccount }
            });
            return !((resp.Success & MAILBOX_DISABLE) > 0 );
        }
    }
}