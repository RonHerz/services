using System.Threading.Tasks;
using BitCom.Common.Models;
using MongoDB.Driver;

namespace BitCom.Common.Repositories
{
    public class UserHandler
    {
        private readonly IMongoDatabase _database;
        public UserHandler(IMongoDatabase pDataBase )
        {
            _database = pDataBase;
        }

        public async Task<UserMongoModel > getUser(string pLogin, string pPassword)
        {
            var builder = Builders<UserMongoModel>.Filter;
            var filter = builder.Eq("Login", pLogin) & builder.Eq("Password", pPassword);
            return await Users.Find(filter).FirstOrDefaultAsync();
        }
        private IMongoCollection<UserMongoModel> Users => _database.GetCollection<UserMongoModel>("Users");
    }
}