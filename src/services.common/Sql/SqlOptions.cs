using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BitCom.Common.Models;
using Microsoft.Extensions.Logging;

namespace Cog.Common.Sql
{
    public class SqlOptions
    {
        public string ConnectionString { get; set; }
    }

    public interface ISqlResult<out T>
    {
        T Success { get; }
        bool IsSuccessful { get; }
        string Error { get; }
    }

    public class SqlResult
    {
        internal static SqlResult<T> Ok<T>(T value)
        {
            return new SqlResult<T>
            {
                IsSuccessful = true,
                Success = value,
                Error = string.Empty
            };
        }

        internal static SqlResult<T> Fail<T>(Exception ex)
        {
            return new SqlResult<T>
            {
                IsSuccessful = false,
                Error = ex.Message
            };
        }
    }

    public class SqlResult<T> : ISqlResult<T>
    {
        public T Success { get; internal set; }
        public bool IsSuccessful { get; internal set; } = true;
        public string Error { get; internal set; }
    }

    public interface ISqlActionsHandler
    {
        Task<List<T>> FillListAsync<T>(string pSpName, Dictionary<string, object> pParams = null, bool pForceOutputParams = false);
        Task<T> ExecSpAsync<T>(string pSpName, Dictionary<string, object> pParams = null);
        Task<int> ExecSpAsync(string pSpName, Dictionary<string, object> pParams = null);
        Task<ISqlResult<T>> ExecuteScalarQueryAsync<T>(string queryStr, Dictionary<string, object> pParams = null);
    }

    public class SqlHandler : ISqlActionsHandler
    {
        private readonly SqlOptions _options;
        protected readonly ILogger<SqlHandler> _logger;
        protected SqlConnection getConnetionByType() => new SqlConnection(_options.ConnectionString);

        public SqlHandler(SqlOptions pOptions, ILogger<SqlHandler> pLog)
        {
            _options = pOptions;
            _logger = pLog;
            pLog.LogInformation("SQL CLIENT CREATED");
        }

        private void SetParameters(SqlCommand cmd, Dictionary<string, object> pParams = null, bool storedProcedure = true)
        {
            if (!storedProcedure)
                cmd.CommandType = CommandType.Text;
            else
                cmd.CommandType = CommandType.StoredProcedure;

            if (pParams != null)
                foreach (var property in pParams.Keys)
                {
                    if (!(pParams[property] == null) && pParams[property].GetType() == typeof(OutPutParameter))
                    {
                        OutPutParameter xParam = (pParams[property] as OutPutParameter);
                        var xOutParam = cmd.Parameters.Add(new SqlParameter(string.Concat("@", property), (pParams[property] as OutPutParameter).Value));
                        xOutParam.Direction = ParameterDirection.InputOutput;
                        if (xParam != null && xParam.IsVarChar)
                        {
                            xOutParam.SqlDbType = SqlDbType.VarChar;
                            xOutParam.Size = xParam.VarcharSize;
                        }
                    }
                    else
                    {
                        var xParam = cmd.Parameters.Add(new SqlParameter(string.Concat("@", property), pParams[property]));
                        xParam.IsNullable = pParams[property] == null;
                        if (xParam.IsNullable)
                            xParam.Value = DBNull.Value;
                        if (pParams[property] is IList && (pParams[property].GetType().GetGenericTypeDefinition() == typeof(List<>)))
                        {
                            var x = ((IList) pParams[property]).Count;
                            if (x == 0) xParam.Value = null;
                            xParam.SqlDbType = SqlDbType.Structured;
                        }
                    }
                }
        }

        protected void SetCommand(SqlCommand cmd, SqlParameter returnValue, Dictionary<string, object> pParams = null)
        {
            SetParameters(cmd, pParams);
            returnValue.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnValue);
        }

        protected async Task<List<T>> ReaderToListAsync<T>(SqlDataReader reader)
        {
            Type _Type = typeof(T);
            var xList = new List<T>();
            var propName = string.Empty;
            try
            {
                int FieldCount = reader.FieldCount;
                while (await reader.ReadAsync())
                {
                    var xInstance = Activator.CreateInstance(_Type);
                    for (int x = 0, f = FieldCount; x < f; x++)
                    {
                        propName = reader.GetName(x);
                        PropertyInfo propertyInfo = xInstance.GetType().GetProperty(propName);
                        //TODO: revisar los nuls y aceptar nulleables del objeto a setear
                        if (propertyInfo != null && propertyInfo.CanWrite) //&& reader.GetValue(x) != DBNull.Value
                        {
                            object value = reader.GetValue(x);
                            propertyInfo.SetValue(xInstance,
                                (value == null || value == DBNull.Value) ? null : Convert.ChangeType(value, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType));
                        }
                    }

                    xList.Add((T) xInstance);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"FieldName: {propName} in ReaderToList: {ex.Message}", ex);
            }

            return xList;
        }

        public async Task<List<T>> FillListAsync<T>(string pSpName, Dictionary<string, object> pParams = null, bool pForceOutputParams = false)
        {
            List<T> xList = new List<T>();
            using (var conn = getConnetionByType())
            using (var cmd = new SqlCommand(pSpName, conn))
                try
                {
                    SetParameters(cmd, pParams);
                    await conn.OpenAsync();
                    using (SqlDataReader dr = await cmd.ExecuteReaderAsync())
                        xList = await ReaderToListAsync<T>(dr);

                    if (pForceOutputParams)
                        foreach (var param in pParams.Values.Where(x => x?.GetType() == typeof(OutPutParameter)))
                        {
                            OutPutParameter xOutPutParam = param as OutPutParameter;
                            xOutPutParam.SetValue(cmd.Parameters[string.Concat("@", xOutPutParam.Name)].Value);
                        }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"FillListAsync<T>:{ex.Message} spName: {pSpName}", ex);
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }

            return xList;
        }

        public async Task<T> ExecSpAsync<T>(string pSpName, Dictionary<string, object> pParams = null)
        {
            Type _Type = typeof(T);
            object xInstance = null;
            using (var conn = getConnetionByType())
            using (var cmd = new SqlCommand(pSpName, conn))
                try
                {
                    SetParameters(cmd, pParams);

                    await conn.OpenAsync();
                    using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        int FieldCount = reader.FieldCount;
                        if (reader.HasRows)
                        {
                            xInstance = Activator.CreateInstance(_Type);
                            while (await reader.ReadAsync())
                            {
                                for (int x = 0, f = FieldCount; x < f; x++)
                                {
                                    PropertyInfo propertyInfo = xInstance.GetType().GetProperty(reader.GetName(x));
                                    if (propertyInfo != null)
                                    {
                                        object value = reader.GetValue(x);
                                        propertyInfo.SetValue(xInstance,
                                            (value == null || value == DBNull.Value) ? null : Convert.ChangeType(value, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType));
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ExecSpAsync<T>: {pSpName} " + ex.Message, ex);
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }

            return (T) xInstance;
        }

        public async Task<int> ExecSpAsync(string pSpName, Dictionary<string, object> pParams = null)
        {
            using (var conn = getConnetionByType())
            using (var cmd = new SqlCommand(pSpName, conn))
                try
                {
                    SqlParameter returnValue = new SqlParameter();
                    SetCommand(cmd, returnValue, pParams);
                    await conn.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                    return (int) returnValue.Value;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ExecSPAsync: STORE PROCEDURE: {pSpName} msg: {ex.Message}", ex);
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }

            return -1;
        }

        public async Task<ISqlResult<T>> ExecuteScalarQueryAsync<T>(string queryStr, Dictionary<string, object> pParams = null)
        {
            using (var conn = getConnetionByType())
            using (var cmd = new SqlCommand(queryStr, conn))
                try
                {
                    SetCommand(cmd, new SqlParameter(), pParams);
                    cmd.CommandType = CommandType.Text;
                    await conn.OpenAsync();
                    var val = await cmd.ExecuteScalarAsync();
                    var ret = val != null ? Convert.ChangeType(val, typeof(T)) : default;
                    return SqlResult.Ok((T) ret);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"ExecuteScalarQuery: {queryStr}, {ex.Message}", ex);
                    return SqlResult.Fail<T>(ex);
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
        }
    }
}