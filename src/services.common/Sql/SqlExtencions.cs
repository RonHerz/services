using Cog.Common.Sql;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace BitCom.Common.Sql
{
    public static class SqlExtencions
    {
        public static void AddSqlServer(this IServiceCollection pServices, IConfiguration pConfig )
        {
            pServices.Configure<SqlOptions>(pConfig.GetSection("sql"));
            pServices.AddSingleton<ISqlActionsHandler,SqlHandler>(c =>
            {
                var options = c.GetService<IOptions<SqlOptions>>();
                var logger = c.GetService<ILogger<SqlHandler>>();
                return new SqlHandler(options.Value,logger);
            });
        }
    }
}