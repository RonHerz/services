using System;
using Microsoft.IdentityModel.JsonWebTokens;

namespace BitCom.Common.Auth
{
    public interface IJwtHandler
    {
        JsonWebToken Create(string IdUser);
    }
}