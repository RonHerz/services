using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace BitCom.Common.Auth
{
    public static class AuthExtencions
    {
        public static void AddJwt(this IServiceCollection pServices, IConfiguration pConfig)
        {
            var options = new JwtOptions();
            var section = pConfig.GetSection("jwt");
            section.Bind(options);
            pServices.Configure<JwtOptions>(section);
            pServices.AddSingleton<IJwtHandler, JwtHandler>();
            pServices.AddAuthentication()
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false,
                        ValidIssuer = options.Issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.SecretKey))
                    };
                });
        }
    }
}