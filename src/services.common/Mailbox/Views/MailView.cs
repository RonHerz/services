using System;
using System.Collections.Generic;
using BitCom.Common.Commands;
using Cog.Common.Commands;

namespace Cog.Common.Mailbox.Views
{
    public enum MAIL_CREATE_TYPE
    {
        NEW = 0,
        REPLY = 1,
        FORWARD = 2,
        ERROR = 3,
        FEEDBACK = 4,
        MANAGEMENT = 5,
        PACKAGE = 6
    }
    public enum MAIL_SPECIAL_SUBJECT
    {
        NONE = 0,
        CUSTOMER_SERVICE_MAIL = 21,
        MANAGEMENT = 23,
        ERROR = 25,
        FEEDBACK = 26,
        PACKAGE = 31,
        ALL_PLAYERS = -999,
        ALL_SUB_AGENTS = -99,
        ALL_AGENTS_PLAYERS = -998,
        CUSTOMER_SERVICE_SEND = -996,
        REPLY_TO_EXTERNAL_EMAIL = -995
    }
    public class MessageView : ICommand
    {
        public MessageView()
        {
            SpecificAccountIds = new List<int>();
        }

        public string Subject { get; set; }
        public string Message { get; set; }
        public int ContactId { get; set; }
        public List<int> SpecificAccountIds { get; set; }
        public int ReferenceMailId { get; set; }
        public bool MustRead { get; set; }
        public DateTime CreatedAt { get; set; }
    }

    public class CommunicationMessageView : MessageView
    {
        public string DirectResponse { get; set; }
        public int FromId { get; set; }
        public int ToId { get; set; }
    }
    public class MailInformation
    {
        public string Ip { get; set; }
        public string Url { get; set; }
        public string TechDetails { get; set; }
    }
    public class MailWrapper : ICommand
    {
        public int IdAgent { get; set; }
        public MAIL_CREATE_TYPE MailType { get; set; }
        public CommunicationMessageView Mail { get; set; }
        public MailInformation Informarion { get; set; }
        protected MailWrapper(){}
        /*public MailWrapper(int pIdAgent,MAIL_CREATE_TYPE pMailType,CommunicationMessageView pMail)
        {
            IdAgent = pIdAgent;
            MailType = pMailType;
            Mail = pMail;
        }*/
    }
}