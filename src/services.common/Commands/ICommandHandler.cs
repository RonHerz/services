using System;
using System.Threading.Tasks;
using Cog.Common.Commands;

namespace BitCom.Common.Commands
{
    public interface ICommandHandler<in T> where T: ICommand
    {
        Task HandleAsync(T command);
    }
}