using System;
using BitCom.Common.Events;
using Cog.Common.Commands;

namespace BitCom.Common.Commands
{
    public class CreateMail : ICommand
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Message { get; set; }
    }
    public class MailAccepted : IEvent
    {
        public DateTime CreatedAt { get; set; }
        public string Message { get; set; } = "message accepted";
    }
}