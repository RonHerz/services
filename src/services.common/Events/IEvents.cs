using System.Threading.Tasks;

namespace BitCom.Common.Events
{
    //Marker interface
    public interface IEvent
    {
    }
    public interface IEventHandler<in T> where T : IEvent
    {
        Task HandleAsync(T @event);
    }
}