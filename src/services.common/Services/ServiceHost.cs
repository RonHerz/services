using System;
using System.Threading.Tasks;
using BitCom.Common.Commands;
using BitCom.Common.Events;
using BitCom.Common.RabbitMq;
using BitCom.Common.Services;
using Cog.Common.Commands;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RawRabbit;

namespace Cog.Common.Services
{
    public class ServiceHost : IServiceHost
    {
        private readonly IWebHost _webHost;

        public ServiceHost(IWebHost pWebHost)
        {
            _webHost = pWebHost;
        }

        public Task Run() => _webHost.RunAsync();

        public static HostBuilder Create<TStarttup>(string[] args) where TStarttup : class
        {
            Console.Title = typeof(TStarttup).Namespace;
            var config = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                //.AddCommandLine()
                .Build();
            var webHostBuilder = WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .ConfigureLogging((hostingContext, logging) =>
                    {
                        // Requires `using Microsoft.Extensions.Logging;`
                        logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                        logging.AddConsole();
                        logging.AddDebug();
                        logging.AddEventSourceLogger();
                    }
                )
                .UseStartup<TStarttup>();
            return new HostBuilder(webHostBuilder.Build());
        }
    }

    public abstract class BuilderBase
    {
        public abstract ServiceHost Build();
    }

    public class HostBuilder : BuilderBase
    {
        private readonly IWebHost _webHost;
        private IBusClient _bus;
        public HostBuilder(IWebHost pWebHost)
        {
            _webHost = pWebHost;
        }
        public BusBuilder UseRabbitMq()
        {
            _bus = (IBusClient)_webHost.Services.GetService(typeof(IBusClient));
            return new BusBuilder(_webHost, _bus);
        }
        public override ServiceHost Build()
        {
            return new ServiceHost(_webHost);
        }
    }
    public class BusBuilder : BuilderBase
    {
        private readonly IWebHost _webHost;
        private IBusClient _bus; 

        public BusBuilder(IWebHost webHost, IBusClient bus)
        {
            _webHost = webHost;
            _bus = bus;
        }

        public BusBuilder SubscribeToCommand<TCommand>() where TCommand : ICommand
        {
            var handler = (ICommandHandler<TCommand>)_webHost.Services
                .GetService(typeof(ICommandHandler<TCommand>));
            
            _bus.WithCommandHandlerAsync(handler);
            return this;
        }

        
        public BusBuilder SubscribeToEvent<TEvent>() where TEvent : IEvent
        {
            var handler = (IEventHandler<TEvent>)_webHost.Services
                .GetService(typeof(IEventHandler<TEvent>));
            _bus.WithEventHandlerAsync(handler);
            return this;
        }
        

        public override ServiceHost Build()
        {
            return new ServiceHost(_webHost);
        }
    }
}