using System.Threading.Tasks;

namespace BitCom.Common.Services
{
    public interface IServiceHost
    {
        Task Run();
    }
}