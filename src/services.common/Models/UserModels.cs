using MongoDB.Bson;

namespace BitCom.Common.Models
{
    public class UserModelBase
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
    public class UserModel
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string EMail { get; set; }
    }

    public class UserLoginRequest :UserModelBase
    {
        
    }
    public class UserMongoModel : UserModel
    {
        public ObjectId  Id { get; set; }         
    }
}