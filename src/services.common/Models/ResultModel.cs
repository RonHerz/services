using System;
using System.Collections.Generic;

namespace Cog.Common.Models
{
    public class Result
    {
        private Result()
        {
        }

        internal static Result<T> Ok<T>(T value)
        {
            return new Result<T>
            {
                Payload = value
            };
        }

        internal static Result<T> Failure<T>(List<string> pErrors)
        {
            return new Result<T>
            {
                Payload = default,
                Error = pErrors
            };
        }
    }

    public class Result<T>
    {
        public T Payload { get; internal set; }
        public List<string> Error { get; set; } = new List<string>();
    }

    public static class ResultHandler
    {
        public static Result<T> Build<T>(Func<T> f) =>f.TryCatch();
    }

    public static class Extencions
    {
        public static Func<T, T> Tee<T>(this Action<T> f)
        {
            return x =>
            {
                f(x);
                return x;
            };
        }
        public static Func<T, Result<V>> Switch<T, V>(this Func<T, V> f)
        {
            return x => Result.Ok(f(x));
        }
        public static Result<T> TryCatch<T>(this Func<T> f)
        {
            try
            {
                return Result.Ok(f());
            }
            catch (Exception exc)
            {
                return Result.Failure<T>(new List<string> {exc.Message});
            }
        }
    }
}