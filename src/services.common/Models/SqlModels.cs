using System;

namespace BitCom.Common.Models
{
    public class OutPutParameterBase<T> {
        private T _Value;
        public OutPutParameterBase (T pValue, string pName) {
            this.Name = pName;
            this._Value = pValue;
        }
        public string Name { get; }
        public T Value { get { return this._Value; } }
        public void SetValue (T value) {
            this._Value = value;
        }
        public virtual int ToInt32 () {
            return Convert.ToInt32 (this._Value);
        }
    }
    public class OutPutParameter : OutPutParameterBase<object> {
        public OutPutParameter (int pValue, string pName,int pVarcharSize = 0) : base (pValue, pName) { 
            this.VarcharSize = pVarcharSize; 
        }
        public bool IsVarChar {get=> this.VarcharSize > 0;}
        public int VarcharSize {get;set;} = 0;
    }
}