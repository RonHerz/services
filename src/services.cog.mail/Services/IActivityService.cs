using System;
using System.Threading.Tasks;

namespace services.cog.messageHandler.Services
{
    public interface IActivityService
    {
        Task AddAsync(Guid id, DateTime createdAt);
    }
}