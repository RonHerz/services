using System.Threading.Tasks;
using BitCom.Common.Commands;
using Cog.Common.Mailbox.Views;
using Cog.Common.Repositories;
using Cog.Common.Sql;
using Microsoft.Extensions.Logging;
using RawRabbit;

namespace services.cog.messageHandler.Handlers.Mail
{
    public class MailHandler : ICommandHandler<MailWrapper> 
    {
        private readonly IBusClient _busClient;
        private readonly ILogger _logger;
        private readonly MailRepositoryBase _repo;
        public MailHandler(IBusClient busClient,ILogger<MailWrapper> logger, ISqlActionsHandler sqlConn )
        {
            _busClient = busClient;
            _logger = logger;
            _repo =new MailRepositoryBase(sqlConn);
        }
        public async Task HandleAsync(MailWrapper command)
        {
            var x =await _repo.IsAllow(command.Mail.ToId);
            _logger.LogInformation($"Allowed: {x} Id Agent: {command.IdAgent}, {command.Mail.Message}");
        }
    }
}