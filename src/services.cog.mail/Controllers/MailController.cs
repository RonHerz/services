﻿using System.Threading.Tasks;
using Cog.Common.Models;
using Cog.Common.Mailbox.Views;
using Microsoft.AspNetCore.Mvc;
using RawRabbit;

namespace services.cog.messageHandler.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {

        public readonly IBusClient _bus;

        public MailController(IBusClient pBus)
        {
            _bus = pBus;
        }
        /*// GET api/values
        [HttpGet("")]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] {"value1"};
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }*/

        [HttpPost("create")]
        public async Task<Result<int>> CreateMail([FromQuery] int IdAgent, [FromBody] CommunicationMessageView pMail)
        {
            await _bus.PublishAsync(pMail);
            return await Task.FromResult(ResultHandler.Build(()=> IdAgent));
        }
    }
}