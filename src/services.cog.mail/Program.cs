﻿using System.Threading.Tasks;
using BitCom.Common.Services;
using Cog.Common.Mailbox.Views;
using Cog.Common.Services;

namespace services.cog.messageHandler
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            await ServiceHost.Create<Startup>(args)
                .UseRabbitMq()
                .SubscribeToCommand<MailWrapper>()
                //.SubscribeToEvent<MailAccepted>()
                .Build()
                .Run();
        }
    }
}