﻿using BitCom.Common.Commands;
using BitCom.Common.RabbitMq;
using BitCom.Common.Sql;
using Cog.Common.Mailbox.Views;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using services.cog.messageHandler.Handlers;
using services.cog.messageHandler.Handlers.Mail;

namespace services.cog.messageHandler
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(opt => {opt.SerializerSettings.ContractResolver = new DefaultContractResolver(); });
            services.AddSqlServer(Configuration);
            services.AddRabbitMq(Configuration);
            services.AddSingleton<ICommandHandler<MailWrapper>, MailHandler>();
            //services.AddSingleton<IEventHandler<MailAccepted>, ActivityMailCreatedHandler>();
            
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}